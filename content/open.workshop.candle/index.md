---

### EXPERIENCE PAGE TEMPLATE
### VERSION: 1
##### LAST UPDATED: 2021-09-17
##### DEVELOPED BY: Robert Doyle

##### Use this template for all experience pages for HUMOS. 

##### Designed to look like a single product page but with a 'book this experience' button instead of 'add to cart' button, this page should be used for all experience-type pages on humos.co.uk 



##### The below settings have descriptive comments and most only generate if present. 

####General page settings

layout: "experience"


###SET WORKSHOP TYPE: options are "open" for open workshops, "individual" for individual blending experience and "private" for private group workshops.

workshop_type: "open"

open_workshop_dates:
###each item should include "title", "date", "time" and "price".
###example of an item:
   1:
      title: Open Candle Making Workshop
      date: 17/09/2021
      time: 3pm
      price: 100

        
slug: "/candle-making-experience-workshop-reading/"
title: "Open candle making workshop in Reading | HUMOS"
description: ""


### page content
experience_title: "Open Candle Making Workshop"

experience_short_description: "Have you ever wished you could create your very own scented candle for that perfect ambience? Our candle making workshop means that you can finally create the candle you’ve always wanted. Learn the secrets behind candle making and choose your very own fragrance with the help of our expert. Our open workshops are a fun, informal way of learning and are open to anyone."

experience_price: "£150"

experience_price_small: "per peson"

###slider images
slider_images:
    1: 
        id: 1
        url: ./images/candle-making-workshop-candle-close-up
        alt_text: Woman smelling fragrance bottle at a HUMOS Open Fragrance Making Workshop in Reading
    2:
        id: 2
        url: ./images/candle-making-workshop-candle-ariel-view
        alt_text: Man holding HUMOS Fragrance Bottle during an open fragrance workshop



###pricing options
pricing:
    title: price
    price: 100
    small: per person 
    individual: "true"
  
  
#######
#ACCORDION
#Accordion at top of page on right side, next to image slider. Script automatically adds new items to accordion based on number of children in object. Maximum of 3 is recommended.
#Icon is font-awesome, search font-awesome website for icon codes.
#######

accordion:
    1:
        id: 1
        icon: "far fa-clock"
        title: "≈ 3.5 hours duration"
        body: "This candle making workshop lasts approximately 3.5 hours."
    2: 
        id: 2
        icon: "far fa-flask-potion"
        title: "Equipment provided"
        body: "All of the equipment necessary to complete your candle will be provided."
    3:
        id: 3
        icon: "far fa-user-friends"
        title: "Limited spaces"
        body: "This open workshop has limited spaces per workshop." 
    4:
        id: 4
        icon: "far fa-flame"
        title: "Candle"
        body: "As part of this open workshop you will be making your own candle."
    5: 
        id: 5
        icon: "far fa-glass-champagne"
        title: "Light refreshments included"
        body: "This experience includes complementory tea, coffee and procescco for you to enjoy whilst blending."

#######
#END ACCORDION
####### 

#######
#FURTHER DETAILS SECTION
#Located just below the image slider at top of page and before the more info section. Oppertunity for text and list. Ensure items in list are evenly distrobuted between list_1 and list_2.
#######

further_details:
    title: "Description"
    content: "Create the candle you've always wanted with this open candle making workshop. Learn the secrets of candle making whilst you create your very own candle with this fun, informal workshop which is open to anyone."
    list:
        title: "Details"
        list_1: 
            1: "Lasts aprox. 3.5 hours"
            2: "Create your own candle"
            3: "Includes all equipment"
        list_2: 
            1: "Guided by Scent Expert"
            2: "Limited spaces"
            3: "Light refreshments included"
 
 #######
 #END FURTHER DETAILS SECTION
 #######
 
 #######
#MORE INFO SECTION
#This section shows more info, what they'll be doing, what's included etc. Script loops to add additional items automatically.
#######

more_info_section:
    1:
        title: "What you'll be doing."
        text: "<p>Working with our expert, you’ll learn how to create your own candle from start to finish. From choosing its fragrance, to hand pouring the wax, you’ll learn how to avoid common pitfalls and discover the secrets behind producing a perfect candle.</p>"
        image_url: "/images/experiences/candle-making/what-you-will-be-doing-candle-workshop.jpg"
        image_alt: "this is the alt for the image"
        text_location: "left"
    2:
        title: "What's included."
        text: "<p>You’ll have expert advice and guidance throughout the session, as well as access to the HUMOS scent library and all the equipment you need to create the perfect candle for your home. </p>"
        image_url: "/images/experiences/candle-making/whats-included-candle-workshop.jpg"
        text_location: "right"


####### 
#END MORE INFO SECTION
#######

#######
#TERMS AND CONDITIONS SECTION
#page specific therms and conditions located at the bottom of the page, this section covers things like booking and cancellation policy.
#######

terms:
    title: "Things to keep in mind"
    text: "<p>All bookings are subject to our cancellation policy. Please see our terms and conditions page.</p><p>There is a minimum booking requirement of 1 and a maximum of 5 persons and all participants will need to be over the age of 18 on the day of the experience.</p><p>Non-participants will not be allowed to wait on-site during the experience. If someone is accompanying you to the experience, we can recommend some local cafes or areas of interest for them to visit whilst you are partaking in the experience.</p><p>Please do not wear any perfume, scented body sprays or scented body lotions on the day of your experience as this can taint the experience for you and other participants.</p><p>Participants with allergies should contact us prior to booking with more details about what allergies they suffer from in order to make sure it is suitable to participate in the experience.</p><p>As this experience includes complimentary Prosecco, valid photo ID may be required.</p><p>We do not allow any outside food and drink to be consumed on-site. Complimentary tea, coffee, water and prosecco is available during the experience and a french patiserie platter can be arranged in advance.</p>"

#######
#END TERMS AND CONDITIONS SECTION
#######
    

---


