---
title: "Our Atelier & Showroom"
description: "Our Atelier & Showroom"
heading: "Our Atelier & Showroom"
subHeading: ""
layout: "two-col"
feature_image: "/images/humos-shop-outline-caversham.jpg"
image_alt_tag : "Our Atelier & Showroom"
---

A Haven of beautiful scents, handmade products and unique experiences.

We have always had a fascination with how an individual’s sense of smell can transport them back in time, trigger a long-lost memory or affect their mood to such a degree it can even change their sense of self.

Nestled in the busy village of Caversham, Berkshire, you’ll find the heart (note) of the HUMOS family. A dedicated, 3-story building housing all the tantalising scents and smells that make Natural Perfumery and Aromatherapy such a wonderful part of life. It is where dreams are created and new ideas are born. Where we develop new scents and experiences and where our unique products are created – all handmade, naturally.

Our Atelier & Showroom in Caversham also serves as the cornerstone for our Natural Perfumery and Home Perfumery Experiences, where you can unleash your creativity, follow your nose and explore the wondrous scents that have made HUMOS a house-hold name. From floral to oriental, clean or romantic, you will have the opportunity to hand-craft your own perfect fusion of natural essences to create your own perfume that truly reflects who you are.