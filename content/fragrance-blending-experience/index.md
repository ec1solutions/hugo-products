---

### EXPERIENCE PAGE TEMPLATE
### VERSION: 1
##### LAST UPDATED: 2021-09-17
##### DEVELOPED BY: Robert Doyle

##### Use this template for all experience pages for HUMOS. 

##### Designed to look like a single product page but with a 'book this experience' button instead of 'add to cart' button, this page should be used for all experience-type pages on humos.co.uk 



##### The below settings have descriptive comments and most only generate if present. 

####General page settings

layout: "experience"


###SET WORKSHOP TYPE: options are "open" for open workshops, "individual" for individual blending experience and "private" for private group workshops.

workshop_type: "individual"

open_workshop_dates:
###each item should include "title", "date", "time" and "price".
###example of an item:
###   1:
###      title: Private Group Fragrance Workshop
###      date: 17/09/2021
###      time: 3pm
###      price: 150

        
slug: "/perfume-fragrance-blending-experience-reading/"
title: "Perfume Fragrance making workshop in Reading | HUMOS"
description: ""


### page content
experience_title: "Fragrance Making Experience"

experience_short_description: "If you’ve always dreamed of creating your own personal perfume, then our fragrance blending experience is for you. No two fragrances are the same because no two people are the same. Your scent will be unique to you."





###slider images
slider_images:
    1: 
        id: 1
        url: ./images/fragrance-blending-experience-reading-humos
        alt_text: "woman smelling frgrance blotters in fragrance making experience at HUMOS."
    2:
        id: 2
        url: ./images/fragrance-blending-experience-reading-humos-07
        alt_text: "woman opening HUMOS bottle during a fragrance perfume making experience in Reading at HUMOS"


###pricing options
pricing:
    title: price
    price: 165
    additional: 
    small: per person. Option to bring a guest, see below.
    min_people: 3
    max_people: 12
    individual: "true"
  
  
#######
#ACCORDION
#Accordion at top of page on right side, next to image slider. Script automatically adds new items to accordion based on number of children in object. Maximum of 3 is recommended.
#Icon is font-awesome, search font-awesome website for icon codes.
#######

accordion:
    1:
        id: 1
        icon: "far fa-clock"
        title: "≈ 1.5 - 2 hours duration"
        body: "This open fragrance workshop lasts approximately 1.5 to 2 hours."
    2: 
        id: 2
        icon: "far fa-flask-potion"
        title: "Equipment provided"
        body: "All of the equipment necessary to complete your fragrance will be provided."
    3:
        id: 3
        icon: "far fa-user-friends"
        title: "Bring a guest options"
        body: "Bring a guest: £60 for a guest not blending or £120 for a guest blending their own fragrance. Maximum 1 guest per person." 
    4:
        id: 4
        icon: "far fa-spray-can"
        title: "15ml fragrance"
        body: "As part of this fragrance making experience you will be making a 15ml bottle of fragrance."
    5: 
        id: 5
        icon: "far fa-glass-champagne"
        title: "Drinks included"
        body: "This experience includes complementory tea, coffee and procescco for you to enjoy whilst blending."

#######
#END ACCORDION
####### 

#######
#FURTHER DETAILS SECTION
#Located just below the image slider at top of page and before the more info section. Oppertunity for text and list. Ensure items in list are evenly distrobuted between list_1 and list_2.
#######

further_details:
    title: "Description"
    content: "If you’ve always dreamed of creating your own personal perfume, then our fragrance blending experience is for you. Making your own perfume can be an intensely personal experience and as such, your fragrance will be unique to you; No two fragrances created with this experience are the same because no two people are the same."
    list:
        title: "Details"
        list_1: 
            1: "Lasts aprox. 1.5 - 2 hours"
            2: "Create a 15ml bottle of fragrance"
            3: "Includes all equipment"
        list_2: 
            1: "Guided by Scent Expert"
            2: "Option to bring a guest"
            3: "Light refreshments included"
 
 #######
 #END FURTHER DETAILS SECTION
 #######
 
 #######
#MORE INFO SECTION
#This section shows more info, what they'll be doing, what's included etc. Script loops to add additional items automatically.
#######


more_info_section:
    1:
        title: "What you'll be doing."
        text: "<p>You'll create, bottle and take home your own personal fragrance that is exclusively yours and created from your carefully chosen materials from the hUMOS library of ingredients."
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included.jpg"
        image_alt: "this is the alt for the image"
        text_location: "left"
    2:
        title: "What's included."
        text: "<p>All equipment needed to create your own personal fragrance is included as is the guidence of our expert will be on hand throughout the process, providing equipment and materials to make your dream perfume into a reality. Light refreshments will be provided. </p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included-2.jpg"
        text_location: "right"
    3:
        title: "Add a little <i>je ne sais quoi.</i>"
        text: "<p>We can arrange for an assortment of French Pastries for you to enjoy on the day at an additional cost. These pastries are hand-crafted from a local French baker and are divine! </p><p>Advanced notice is required and cannot guarantee they will be free from allergens.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/french-pastries-humos-caveresham.jpg"
        text_location: "left"

####### 
#END MORE INFO SECTION
#######

#######
#TERMS AND CONDITIONS SECTION
#page specific therms and conditions located at the bottom of the page, this section covers things like booking and cancellation policy.
#######

terms:
    title: "Things to keep in mind"
    text: "<p>All bookings are subject to our cancellation policy. Please see our terms and conditions page.</p><p>There is a minimum booking requirement of 1 and a maximum of 5 persons and all participants will need to be over the age of 18 on the day of the experience.</p><p>Non-participants will not be allowed to wait on-site during the experience. If someone is accompanying you to the experience, we can recommend some local cafes or areas of interest for them to visit whilst you are partaking in the experience.</p><p>Please do not wear any perfume, scented body sprays or scented body lotions on the day of your experience as this can taint the experience for you and other participants.</p><p>Participants with allergies should contact us prior to booking with more details about what allergies they suffer from in order to make sure it is suitable to participate in the experience.</p><p>As this experience includes complimentary Prosecco, valid photo ID may be required.</p><p>We do not allow any outside food and drink to be consumed on-site. Complimentary tea, coffee, water and prosecco is available during the experience and a french patiserie platter can be arranged in advance.</p>"

#######
#END TERMS AND CONDITIONS SECTION
#######
    

---


