---
title: "Nuestro estudio y sala de exposiciones"
description: "Nuestro estudio y sala de exposiciones"
heading: "Nuestro estudio y sala de exposiciones"
subHeading: ""
layout: "two-col"
feature_image: "/images/humos-shop-outline-caversham.jpg"
image_alt_tag : "Nuestro estudio y sala de exposiciones"
slug: "/taller-y-sala-de-exposiciones-madrid/"
---

Un refugio de bellos aromas, productos hechos a mano y experiencias únicas.

Siempre nos ha fascinado cómo el sentido del olfato de un individuo puede transportarlo al pasado, desencadenar un recuerdo perdido hace mucho tiempo o afectar su estado de ánimo hasta tal punto que puede incluso cambiar su sentido de sí mismo.

Ubicado en el ajetreado pueblo de Caversham, Berkshire, encontrará el corazón (nota) de la familia HUMOS. Un edificio dedicado de 3 pisos que alberga todos los aromas y olores tentadores que hacen de la Perfumería y Aromaterapia naturales una parte tan maravillosa de la vida. Es donde se crean los sueños y nacen nuevas ideas. Donde desarrollamos nuevos aromas y experiencias y donde se crean nuestros productos únicos, todos hechos a mano, naturalmente.

Nuestro Atelier & Showroom en Caversham también sirve como la piedra angular de nuestras Experiencias de Perfumería Natural y Perfumería Doméstica, donde puede dar rienda suelta a su creatividad, seguir su olfato y explorar los maravillosos aromas que han hecho de HUMOS un nombre familiar. De floral a oriental, limpio o romántico, tendrá la oportunidad de crear a mano su propia fusión perfecta de esencias naturales para crear su propio perfume que realmente refleje quién es usted.