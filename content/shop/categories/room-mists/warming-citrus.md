---
title: "Warming Citrus Essential Oil Room Mist | HUMOS"
description: "HUMOS Warming Citrus Room Mist is a natural, blended aromatherapy essential oil room mist handmade in Reading, Berkshire by HUMOS Natural Perfumery"
parent_url: "room-mists"

product_id: "WARMING_CITRUS_ROOM_MIST"
layout: "product"
slug: "/warming-citrus-essential-oil-room-mist/"


more_details_image_1: "/images/essential-oil-room-mist-bottle-zoom-750x750.jpg"
---

{{< rawhtml >}}

<h3 class="mb-4">Warming Citrus.</h3>
<p>
A Sweet and exotic scent with a warming kick. Features Sweet orange, Ginger and Patchouli essential oils.

</p>
<p>Layer with our subtle yet powerful <a href="#" class="btn-link text-decoration-none text-inherit">Warming Citrus Essential Oil Candle</a> for the ultimate home-filling scent.</p>

{{< /rawhtml >}}

||||||||||

{{< rawhtml >}}

<h3 class="mb-4">Blended by hand in small batches.</h3>
<p>All of our room mists are expertly formulated and blended in-house using the purest possible essential oils.</p>

<p>These oil blends are then suspended in a vegan, eco-solvent that's both good for the planet and your home before being carefully and lovingly bottled for you to enjoy.</p>

{{< /rawhtml >}}