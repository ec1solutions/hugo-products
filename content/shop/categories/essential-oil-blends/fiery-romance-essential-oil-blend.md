---
title: "Fiery Romance Pure Essential Oil Blend | HUMOS"
description: "HUMOS Fiery Romance Blended Essential oil is a natural, blended aromatherapy essential oil handmade in Reading, Berkshire by HUMOS Natural Perfumery"
parent_url: "essential-oil-blends"

product_id: "FIERY_ROMANCE_BLENDED_ESSENTIAL_OIL"
layout: "product"
slug: "/fiery-romance-essential-oil-blend/"


more_details_image_1: "/images/essential-oil-blend-bottle-zoom-750x750.jpg"
more_details_image_2: "/images/blended-essential-oil-feature-image.jpg"

---

{{< rawhtml >}}
<h3 class="mb-4">Fiery Romance.</h3>
<p>
Add a little spice with a blend of Patchouli, Cade, Fir needle, Cinnamon, spearmint and clove bud essential oils. 
</p>
<p>Layer with our subtle yet powerful <a href="#" class="btn-link text-decoration-none text-inherit">Smoked Wood Essential Oil Candle</a> for the ultimate home-filling scent.</p>
<p></p>
{{< /rawhtml >}}

||||||||||

{{< rawhtml >}}
<h3 class="mb-4">Hand-blended in small batches.</h3>
<p>Our essential oil blends are expertly formulated and hand-blended in-house using the purest possible essential oils in small batches.</p>
<p></p>


{{< /rawhtml >}}

||||||||||

{{< rawhtml >}}
<h3 class="mb-4">Therapeutic Benefits</h3>
<p>All of our room mists are expertly formulated and blended in-house using the purest possible essential oils.</p>

<p>These oil blends are then suspended in a vegan, eco-solvent that's both good for the planet and your home before being carefully and lovingly bottled for you to enjoy.</p>
{{< /rawhtml >}}