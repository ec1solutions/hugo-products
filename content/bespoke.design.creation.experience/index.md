---

### EXPERIENCE PAGE TEMPLATE
### VERSION: 1
##### LAST UPDATED: 2021-09-17
##### DEVELOPED BY: Robert Doyle

##### Use this template for all experience pages for HUMOS. 

##### Designed to look like a single product page but with a 'book this experience' button instead of 'add to cart' button, this page should be used for all experience-type pages on humos.co.uk 



##### The below settings have descriptive comments and most only generate if present. 

####General page settings

layout: "experience"


###SET WORKSHOP TYPE: options are "open" for open workshops, "individual" for individual blending experience and "private" for private group workshops.

workshop_type: "private"

open_workshop_dates:
###each item should include "title", "date", "time" and "price".
###example of an item:
###   1:
###      title: Private Group Fragrance Workshop
###      date: 17/09/2021
###      time: 3pm
###      price: 150

        
slug: "/bespoke-fragrance-design-and-creation-experience/"
title: "Bespoke Fragrance Design and Creation Experience | HUMOS"
description: ""


### page content
experience_title: "Bespoke Design and Creation Experience"

experience_short_description: "Create your own signature scent inspired by your deepest emotions and memories: a fragrance that nobody else will own. With the guidance of your own personal fragrance expert, learn the secrets behind the fascinating world of perfumery. This is a private session, booked for your exclusive experience, suitable for between one to three people."





###slider images
slider_images:
    1: 
        id: 1
        url: ./images/bespoke-fragrance-creation-experience
        alt_text: Perfumer mixing fragrance notes during bespoke creation experience
    2:
        id: 2
        url: ./images/perfume-creation-in-reading
        alt_text: creating a bespoke perfume in Reading
    3:
        id: 3
        url: ./images/group-workshop-perfume-making
        alt_text: measuring fragrance from dropper bottle during a fragrance and perfume creation experience



###pricing options
pricing:
    individual: "true"
    title: price
    price: 395
    additional: 395
    small: "per person"
    min_people: 1
    max_people: 3
  
  
#######
#ACCORDION
#Accordion at top of page on right side, next to image slider. Script automatically adds new items to accordion based on number of children in object. Maximum of 3 is recommended.
#Icon is font-awesome, search font-awesome website for icon codes.
#######

accordion:
    1:
        id: 1
        icon: "far fa-clock"
        title: "≈ 3 hours duration"
        body: "This bespoke design and creation experience lasts approximately 3 hours."
    2: 
        id: 2
        icon: "far fa-flask-potion"
        title: "Equipment provided"
        body: "All of the equipment necessary to complete your fragrance will be provided."
    3:
        id: 3
        icon: "far fa-user-friends"
        title: "1 - 3 people"
        body: "This experience can only be booked individually or as a group of up to 3 people." 
    4:
        id: 4
        icon: "far fa-spray-can"
        title: "50ml fragrance"
        body: "As part of this open workshop you will be blending a 50ml bottle of fragrance."
    5: 
        id: 5
        icon: "far fa-glass-champagne"
        title: "Drinks included"
        body: "This experience includes complementory tea, coffee and procescco for you to enjoy during the experience."

#######
#END ACCORDION
####### 

#######
#FURTHER DETAILS SECTION
#Located just below the image slider at top of page and before the more info section. Oppertunity for text and list. Ensure items in list are evenly distrobuted between list_1 and list_2.
#######

further_details:
    title: "Description"
    content: "This is a private session, booked for your exclusive experience, suitable for between one to three people."
    list:
        title: "Further Details"
        list_1: 
            1: "Lasts aprox. 3 hours"
            2: "Create a 50ml bottle of fragrance"
            3: "Includes all equipment"
        list_2: 
            1: "Guided by Perfumer and Scent Designer"
            2: "1 - 3 persons"
            3: "Light refreshments included"
 
 #######
 #END FURTHER DETAILS SECTION
 #######
 
 #######
#MORE INFO SECTION
#This section shows more info, what they'll be doing, what's included etc. Script loops to add additional items automatically.
#######

more_info_section:
    1:
        title: "What you'll be doing."
        text: "<p>Delight your senses as you embark on a journey to find your own bespoke fragrance.  Your personal perfume will be created step by step, using carefully selected ingredients. From the rare and costly to popular favourites, choose from a collection of nearly three hundred ingredients, both natural and synthetic. From dipping the very first scent strip to bottling the finished product, you’ll be hands-on every step of the way.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included.jpg"
        image_alt: "this is the alt for the image"
        text_location: "left"
    2:
        title: "What's included."
        text: "<p>You’ll benefit from your own personal fragrance expert as well as the use of the HUMOS ingredients library. Your preference of light refreshments will be served throughout the session, from tea or coffee to chilled prosecco. Your bespoke fragrance is presented to you in a gift bag at the end of an unforgettable evening.  </p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included-2.jpg"
        text_location: "right"
    3:
        title: "Add a little <i>je ne sais quoi.</i>"
        text: "<p>We can arrange for an assortment of French Pastries for you to enjoy on the day at an additional cost. These pastries are hand-crafted from a local French baker and are divine! </p><p>Advanced notice is required and cannot guarantee they will be free from allergens.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/french-pastries-humos-caveresham.jpg"
        text_location: "left"

####### 
#END MORE INFO SECTION
#######

#######
#TERMS AND CONDITIONS SECTION
#page specific therms and conditions located at the bottom of the page, this section covers things like booking and cancellation policy.
#######

terms:
    title: "Things to keep in mind"
    text: "<p>All bookings are subject to our cancellation policy. Please see our terms and conditions page.</p><p>There is a minimum booking requirement of 1 and a maximum of 5 persons and all participants will need to be over the age of 18 on the day of the experience.</p><p>Non-participants will not be allowed to wait on-site during the experience. If someone is accompanying you to the experience, we can recommend some local cafes or areas of interest for them to visit whilst you are partaking in the experience.</p><p>Please do not wear any perfume, scented body sprays or scented body lotions on the day of your experience as this can taint the experience for you and other participants.</p><p>Participants with allergies should contact us prior to booking with more details about what allergies they suffer from in order to make sure it is suitable to participate in the experience.</p><p>As this experience includes complimentary Prosecco, valid photo ID may be required.</p><p>We do not allow any outside food and drink to be consumed on-site. Complimentary tea, coffee, water and prosecco is available during the experience and a french patiserie platter can be arranged in advance.</p>"

#######
#END TERMS AND CONDITIONS SECTION
#######
    

---


