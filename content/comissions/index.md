---

### EXPERIENCE PAGE TEMPLATE
### VERSION: 1
##### LAST UPDATED: 2021-09-17
##### DEVELOPED BY: Robert Doyle

##### Use this template for all experience pages for HUMOS. 

##### Designed to look like a single product page but with a 'book this experience' button instead of 'add to cart' button, this page should be used for all experience-type pages on humos.co.uk 



##### The below settings have descriptive comments and most only generate if present. 

####General page settings

layout: "bespoke"


###SET WORKSHOP TYPE: options are "open" for open workshops, "individual" for individual blending experience and "private" for private group workshops.

workshop_type: "bespoke"

        
slug: "/commission-a-fragrance/"
title: "commission a bespoke fragrance | HUMOS"
description: ""


### page content
experience_title: "Fragrance Commissions"

experience_short_description: "With a commissioned fragrance you’ll be able to capture a dream and bring it to life, bottle a memory or moment and re-live it forever or even create something that words simply cannot define. A commissioned fragrance from HUMOS offers you limitless possibilities and opportunities to create your defining fragrance, one that no one has ever worn or will ever wear and is completely unique to you."





###slider images
slider_images:
    1: 
        id: 1
        url: ./images/bespoke-fragrance-creation-experience
        alt_text: Perfumer mixing fragrance notes during bespoke creation experience
    2:
        id: 2
        url: ./images/perfume-creation-in-reading
        alt_text: creating a bespoke perfume in Reading
    3:
        id: 3
        url: ./images/group-workshop-perfume-making
        alt_text: measuring fragrance from dropper bottle during a fragrance and perfume creation experience



###pricing options
pricing:
    individual: "true"
    title: price
    price: Varies
    additional:
    small:
    min_people: 
    max_people:
  
  
#######
#ACCORDION
#Accordion at top of page on right side, next to image slider. Script automatically adds new items to accordion based on number of children in object. Maximum of 3 is recommended.
#Icon is font-awesome, search font-awesome website for icon codes.
#######

accordion:
    1:
        id: 1
        icon: "far fa-clock"
        title: "varies"
        body: "The time to complete a fragrance commission varies on the complexity and requirements."
    2:
        id: 2
        icon: "far fa-spray-can"
        title: "15 or 50ml fragrance"
        body: "Depending on the package you choose, you will be presented with a 15ml or 50ml bottle of bespoke fragrance."


#######
#END ACCORDION
####### 

#######
#FURTHER DETAILS SECTION
#Located just below the image slider at top of page and before the more info section. Oppertunity for text and list. Ensure items in list are evenly distrobuted between list_1 and list_2.
#######

further_details:
    title: "Description"
    content: "A commissioned fragrance from HUMOS offers you limitless possibilities and opportunities to create your defining fragrance, one that no one has ever worn or will ever wear and is completely unique to you."
    list:
        title: "Further Details"
        list_1: 
            1: "15ml or 50ml bottle of fragrance"
            2: "Designed and made for you"
        list_2: 
            1: "Created by our perfumer"

 
 #######
 #END FURTHER DETAILS SECTION
 #######
 
 #######
#MORE INFO SECTION
#This section shows more info, what they'll be doing, what's included etc. Script loops to add additional items automatically.
#######

more_info_section:
    1:
        title: "What you'll be doing."
        text: "<p>Every dream begins with a story. Our fragrance expert will listen to yours. Delve deep to bring your deepest emotions alive, reach into your sweetest memories and set them free. Capture that precious moment in a scent you can truly call your own. </p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included.jpg"
        image_alt: "this is the alt for the image"
        text_location: "left"
    2:
        title: "What's included."
        text: "<p>Depending on which package you choose, you will be presented with a 15ml or 50ml bottle of bespoke fragrance, created to meet your exact specifications. Your dream fragrance is yours, captured forever in a bottle. Your secret formula will be kept on file for your exclusive use only.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included-2.jpg"
        text_location: "right"


####### 
#END MORE INFO SECTION
#######

#######
#TERMS AND CONDITIONS SECTION
#page specific therms and conditions located at the bottom of the page, this section covers things like booking and cancellation policy.
#######

terms:
    title: "Things to keep in mind"
    text: "<p>All bookings are subject to our cancellation policy. Please see our terms and conditions page.</p><p>There is a minimum booking requirement of 1 and a maximum of 5 persons and all participants will need to be over the age of 18 on the day of the experience.</p><p>Non-participants will not be allowed to wait on-site during the experience. If someone is accompanying you to the experience, we can recommend some local cafes or areas of interest for them to visit whilst you are partaking in the experience.</p><p>Please do not wear any perfume, scented body sprays or scented body lotions on the day of your experience as this can taint the experience for you and other participants.</p><p>Participants with allergies should contact us prior to booking with more details about what allergies they suffer from in order to make sure it is suitable to participate in the experience.</p><p>As this experience includes complimentary Prosecco, valid photo ID may be required.</p><p>We do not allow any outside food and drink to be consumed on-site. Complimentary tea, coffee, water and prosecco is available during the experience and a french patiserie platter can be arranged in advance.</p>"

#######
#END TERMS AND CONDITIONS SECTION
#######
    

---


