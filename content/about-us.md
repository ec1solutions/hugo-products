---
title: "ABOUT US"
description: "This is about"
heading: "Hello, we're HUMOS"
subHeading: ""
layout: "two-col"
feature_image: "/images/essential-oil-therapeutic-benefits-feature-750x750.jpg"
image_alt_tag : "Hello, we're HUMOS"
permalink: /about-us/
---

ENGLISHWe are a Natural Perfumery and Aromatherapy Chandler situated in heart of Royal Berkshire and we make and sell beautiful things that smell amazing! We also help people create their own Natural Scents with our Blend Bar Experiences and Group Workshops.

We have always had a fascination with how an individual’s sense of smell can transport them back in time, trigger a long-lost memory or affect their mood to such a degree it can even change their sense of self.

Founded in 2015, HUMOS began a journey of sharing the unique scent creations of Scent Expert and Company Founder, Gabriel De Carvalho, through a range of bespoke, handmade aromatherapy treatment candles to help enrich the lives of his clients.

This laid the foundation for the opening of our Atelier & Showroom in Caversham in 2017, where you can sample and purchase our range of original aromatherapy treatment candles, blended essential oils, room mists and sprays or explore your own scent journey with one of our Blending Experiences or aromachology treatments.

All of our products are are a true labour of love, handmade at our Atelier & Showroom in Caversham by our small team of dedicated individuals who share our passion for great scents, naturally.dffsd

gfdfdgsfgsdfg


