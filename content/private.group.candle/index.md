---

### EXPERIENCE PAGE TEMPLATE
### VERSION: 1
##### LAST UPDATED: 2021-09-17
##### DEVELOPED BY: Robert Doyle

##### Use this template for all experience pages for HUMOS. 

##### Designed to look like a single product page but with a 'book this experience' button instead of 'add to cart' button, this page should be used for all experience-type pages on humos.co.uk 



##### The below settings have descriptive comments and most only generate if present. 

####General page settings

layout: "experience"


###SET WORKSHOP TYPE: options are "open" for open workshops, "individual" for individual blending experience and "private" for private group workshops.

workshop_type: "private"

open_workshop_dates:
###each item should include "title", "date", "time" and "price".
###example of an item:
###   1:
###      title: Private Group candle making Workshop
###      date: 17/09/2021
###      time: 3pm
###      price: 150

        
slug: "/private-group-candle-making-workshop-experience-reading/"
title: "Private Group candle making workshop in Reading | HUMOS"
description: ""


### page content
experience_title: "Private Group candle making Workshop"

experience_short_description: "If you’d prefer an exclusive workshop with your own guests, then our Private Candle Making Workshop is for you. A private workshop can be booked for a maximum of five guests.  Create the candle you’ve always wanted, but could never find. Learn the secrets behind candle making and choose your very own fragrance with the help of our expert."





###slider images
slider_images:
    1: 
        id: 1
        url: ./images/reed-diffuser-equipment-in-workshop-at-reading
        alt_text: Group using equipment and bottles as part of a home fragrance reed diffuser and spray making experience
    2:
        id: 2
        url: ./images/reed-diffuser-making-experience
        alt_text: Woman smelling scent from dropper bottle in reed diffuser making experience
    3:
        id: 3
        url: ./images/reed-diffuser-home-spray-workshop-reading
        alt_text: Group of friends making reed diffusers and home sprays during a fragrance making workshop in Reading.
    4:
        id: 4
        url: ./images/reed-diffuser-and-home-spray-making-experience
        alt_text: "HUMOS reed diffuser and home spray bottle from workshop"


###pricing options

pricing:
    title: price
    price: 590
    additional: 110
    small: for up to 3 people.  £1 per additional person.
    min_people: 3
    max_people: 8
  
  
#######
#ACCORDION
#Accordion at top of page on right side, next to image slider. Script automatically adds new items to accordion based on number of children in object. Maximum of 3 is recommended.
#Icon is font-awesome, search font-awesome website for icon codes.
#######

accordion:
    1:
        id: 1
        icon: "far fa-clock"
        title: "≈ 3.5 hours duration"
        body: "This private candle workshop lasts approximately 3 hours."
    2: 
        id: 2
        icon: "far fa-flask-potion"
        title: "Equipment provided"
        body: "All of the equipment necessary to create your candle will be provided."
    3:
        id: 3
        icon: "far fa-user-friends"
        title: "Min. 3 people, Max. 5 people"
        body: "Price includes up to 3 people. Additional attendees can be added for £110 per additional person up to a maximum of 8 attendees." 
    4:
        id: 4
        icon: "far fa-flame"
        title: "Candle"
        body: "As part of this private candle making workshop you will and your guests will each be creating your very own candle."
    5: 
        id: 5
        icon: "far fa-glass-champagne"
        title: "Light refreshments included"
        body: "This experience includes complementory tea, coffee and procescco for you to enjoy whilst blending."

#######
#END ACCORDION
####### 

#######
#FURTHER DETAILS SECTION
#Located just below the image slider at top of page and before the more info section. Oppertunity for text and list. Ensure items in list are evenly distrobuted between list_1 and list_2.
#######

further_details:
    title: "Description"
    content: "An exclusive workshop with your own guests. Create the candle you've always wanted with this exclusive workshop with your own guests whilst learning the secrets behind candle making and how to choose your very own fragrance."
    list:
        title: "Details"
        list_1: 
            1: "Lasts aprox. 3 .5hours"
            2: "Exclusive private workshop"
            3: "Includes all equipment"
        list_2: 
            1: "Guided by Scent Expert"
            2: "Min. 3, Max. 5 people"
            3: "Light refreshments included"
 
 #######
 #END FURTHER DETAILS SECTION
 #######
 
 #######
#MORE INFO SECTION
#This section shows more info, what they'll be doing, what's included etc. Script loops to add additional items automatically.
#######

more_info_section:
    1:
        title: "What you'll be doing."
        text: "<p>Working with our expert, you’ll learn how to create your own candle from start to finish. From choosing its fragrance, to hand pouring the wax, you’ll learn how to avoid common pitfalls and discover the secrets behind producing a perfect candle.</p>"
        image_url: "/images/experiences/candle-making/what-you-will-be-doing-candle-workshop.jpg"
        image_alt: "this is the alt for the image"
        text_location: "left"
    2:
        title: "What's included."
        text: "<p>You’ll have expert advice and guidance throughout the session, as well as access to the HUMOS scent library and all the equipment you need to create the perfect candle for your home.</p>"
        image_url: "/images/experiences/candle-making/whats-included-candle-workshop.jpg"
        text_location: "right"
    3:
        title: "Add a little <i>je ne sais quoi.</i>"
        text: "<p>We can arrange for an assortment of French Pastries for you to enjoy on the day at an additional cost. These pastries are hand-crafted from a local French baker and are divine! </p><p>Advanced notice is required and cannot guarantee they will be free from allergens.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/french-pastries-humos-caveresham.jpg"
        text_location: "left"

####### 
#END MORE INFO SECTION
#######

#######
#TERMS AND CONDITIONS SECTION
#page specific therms and conditions located at the bottom of the page, this section covers things like booking and cancellation policy.
#######

terms:
    title: "Things to keep in mind"
    text: "<p>All bookings are subject to our cancellation policy. Please see our terms and conditions page.</p><p>There is a minimum booking requirement of 1 and a maximum of 5 persons and all participants will need to be over the age of 18 on the day of the experience.</p><p>Non-participants will not be allowed to wait on-site during the experience. If someone is accompanying you to the experience, we can recommend some local cafes or areas of interest for them to visit whilst you are partaking in the experience.</p><p>Please do not wear any perfume, scented body sprays or scented body lotions on the day of your experience as this can taint the experience for you and other participants.</p><p>Participants with allergies should contact us prior to booking with more details about what allergies they suffer from in order to make sure it is suitable to participate in the experience.</p><p>As this experience includes complimentary Prosecco, valid photo ID may be required.</p><p>We do not allow any outside food and drink to be consumed on-site. Complimentary tea, coffee, water and prosecco is available during the experience and a french patiserie platter can be arranged in advance.</p>"

#######
#END TERMS AND CONDITIONS SECTION
#######
    

---


