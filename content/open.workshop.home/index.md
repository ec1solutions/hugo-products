---

### EXPERIENCE PAGE TEMPLATE
### VERSION: 1
##### LAST UPDATED: 2021-09-17
##### DEVELOPED BY: Robert Doyle

##### Use this template for all experience pages for HUMOS. 

##### Designed to look like a single product page but with a 'book this experience' button instead of 'add to cart' button, this page should be used for all experience-type pages on humos.co.uk 



##### The below settings have descriptive comments and most only generate if present. 

####General page settings

layout: "experience"


###SET WORKSHOP TYPE: options are "open" for open workshops, "individual" for individual blending experience and "private" for private group workshops.

workshop_type: "open"

open_workshop_dates:
###each item should include "title", "date", "time" and "price".
###example of an item:
   1:
      title: Open Home Fragrance Workshop
      date: 17/09/2021
      time: 3pm
      price: 100

        
slug: "/open-home-fragrance-workshop-experience-reading/"
title: "Open home Fragrance reed diffuser and room mist workshop in Reading | HUMOS"
description: ""


### page content
experience_title: "Open Home Fragrance Workshop"

experience_short_description: "If you’re seeking a fun, informal way of creating your own home fragrance then our open home fragrance workshops are for you. Discover the secrets behind creating a long lasting and unique home scent; a scent that says ‘home’ like no other. Open home fragrance workshops are on a set time and date, and anyone can book a place. "

experience_price: "£100"

experience_price_small: "per peson"

###slider images
slider_images:
    1: 
        id: 1
        url: ./images/open-home-fragrance-workshop-bottle-reading
        alt_text: Women smelling perfume bottle and blotter workshop making perfume in Reading
    2:
        id: 2
        url: ./images/open-home-fragrance-workshop-bottle-reading
        alt_text: Woman lifting fragrance dropper bottle during workshop
    3:
        id: 3
        url: ./images/open-home-fragrance-workshop-diffuser-spray
        alt_text: fragrance workshop bottles on shelf base ingredients perfume in reading 


###pricing options
pricing:
    title: price
    price: 100
    small: per person 
  
  
#######
#ACCORDION
#Accordion at top of page on right side, next to image slider. Script automatically adds new items to accordion based on number of children in object. Maximum of 3 is recommended.
#Icon is font-awesome, search font-awesome website for icon codes.
#######

accordion:
    1:
        id: 1
        icon: "far fa-clock"
        title: "≈ 3.5 hours duration"
        body: "This open home fragrance workshop lasts approximately 3.5 hours."
    2: 
        id: 2
        icon: "far fa-flask-potion"
        title: "Equipment provided"
        body: "All of the equipment necessary to complete your fragrance will be provided."
    3:
        id: 3
        icon: "far fa-user-friends"
        title: "Limited spaces"
        body: "This open workshop has limited spaces per workshop." 
    4:
        id: 4
        icon: "far fa-spray-can"
        title: "100ml Diffuser and Spray"
        body: "As part of this open workshop you will be creating your own 100ml reed diffuser and home spray."
    5: 
        id: 5
        icon: "far fa-glass-champagne"
        title: "Drinks included"
        body: "This experience includes complementory tea, coffee and procescco for you to enjoy whilst blending."

#######
#END ACCORDION
####### 

#######
#FURTHER DETAILS SECTION
#Located just below the image slider at top of page and before the more info section. Oppertunity for text and list. Ensure items in list are evenly distrobuted between list_1 and list_2.
#######

further_details:
    title: "Description"
    content: "A fun, informal way of creating your own home fragrance pieces. Discover the secrets behind creating a long lasting and unique home scent  that says ‘home’ like no other. This open home fragrance workshop is on set time and dates, and anyone can book a place."
    list:
        title: "Details"
        list_1: 
            1: "Lasts aprox. 3.5 hours"
            2: "Create a reed diffuser and room mist"
            3: "Includes all equipment"
        list_2: 
            1: "Guided by Scent Expert"
            2: "Limited spaces"
            3: "Light refreshments included"
 
 #######
 #END FURTHER DETAILS SECTION
 #######
 
 #######
#MORE INFO SECTION
#This section shows more info, what they'll be doing, what's included etc. Script loops to add additional items automatically.
#######

more_info_section:
    1:
        title: "What you'll be doing."
        text: "<p>You’ll be working closely with our scent expert to realise your dream of a bespoke home fragrance. Delight your nose and surprise your senses with our range of high-quality ingredients; some you may recognise, and some you might discover for the first time.  At the end of the session, you’ll leave with a reed diffuser and room spray made to your exact requirements, hand made by you: an unforgettable sensory experience.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included.jpg"
        image_alt: "this is the alt for the image"
        text_location: "left"
    2:
        title: "What's included."
        text: "<p>You’ll benefit from guidance and advice from our scent expert as well as enjoying access to a vast library of high-quality ingredients. Light refreshments are included. You’ll take home two home fragrance products, exclusive to you and you alone.</p>"
        image_url: "/images/experiences/fragrance-blending-experience/fragrance-blending-experience-whats-included-2.jpg"
        text_location: "right"

####### 
#END MORE INFO SECTION
#######

#######
#TERMS AND CONDITIONS SECTION
#page specific therms and conditions located at the bottom of the page, this section covers things like booking and cancellation policy.
#######

terms:
    title: "Things to keep in mind"
    text: "<p>All bookings are subject to our cancellation policy. Please see our terms and conditions page.</p><p>There is a minimum booking requirement of 1 and a maximum of 5 persons and all participants will need to be over the age of 18 on the day of the experience.</p><p>Non-participants will not be allowed to wait on-site during the experience. If someone is accompanying you to the experience, we can recommend some local cafes or areas of interest for them to visit whilst you are partaking in the experience.</p><p>Please do not wear any perfume, scented body sprays or scented body lotions on the day of your experience as this can taint the experience for you and other participants.</p><p>Participants with allergies should contact us prior to booking with more details about what allergies they suffer from in order to make sure it is suitable to participate in the experience.</p><p>As this experience includes complimentary Prosecco, valid photo ID may be required.</p><p>We do not allow any outside food and drink to be consumed on-site. Complimentary tea, coffee, water and prosecco is available during the experience and a french patiserie platter can be arranged in advance.</p>"

#######
#END TERMS AND CONDITIONS SECTION
#######
    

---


